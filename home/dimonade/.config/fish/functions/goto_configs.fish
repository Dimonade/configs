function goto_configs --wraps='cd ~/.config' --description 'alias goto_configs cd ~/.config'
  cd ~/.config $argv; 
end
