function goto_repos --wraps='cd ~/repositories' --description 'alias goto_repos cd ~/repositories'
  cd ~/repositories $argv; 
end
