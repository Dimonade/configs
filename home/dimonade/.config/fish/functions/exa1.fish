function exa1 --wraps='exa -1lFaT --level=1' --description 'alias exa1=exa -1lFaT --level=1'
  exa -1lFaT --level=1 $argv
        
end
