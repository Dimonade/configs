function goto_nvim --wraps='cd ~/.config/nvim' --description 'alias goto_nvim cd ~/.config/nvim'
  cd ~/.config/nvim $argv; 
end
