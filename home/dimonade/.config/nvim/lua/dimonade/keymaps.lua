local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

-- Shorten function name.
local set_keymap = vim.api.nvim_set_keymap

-- Remap Space as a Leader Key.
set_keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Bufferline.
set_keymap("n", "<S-l>", ":bnext<CR>", opts)
set_keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- Normal mode.
-- Move text up and down.
vim.keymap.set("n", "J", "<Esc>:m .+1<CR>==gi", opts)
vim.keymap.set("n", "K", "<Esc>:m .-2<CR>==gi", opts)

-- Visual mode.
-- Stay in indent mode.
set_keymap("v", "<", "<gv", opts)
set_keymap("v", ">", ">gv", opts)

-- Move text up and down.
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Visual Block mode.
-- Move text up and down.
vim.keymap.set("x", "J", ":move '>+1<CR>gv-gv")
vim.keymap.set("x", "K", ":move '<-2<CR>gv-gv")

-- When jumping half-page, keep cursor in center.
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
-- When jumping between searches, keep cursor in center.
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
-- Paste over without losing value from register.
vim.keymap.set("x", "<leader>p", "\"_dP")
-- Yank to global or local clipboard.
-- Good as <leader>yap to yank a paragraph.
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")
