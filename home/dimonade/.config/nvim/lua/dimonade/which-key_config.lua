local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
	vim.notify("which-key has failed to initialize.")
	return
end

local setup = {
	plugins = {
		marks = true, -- shows a list of your marks on ' and `
		registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
		spelling = {
			enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
			suggestions = 32, -- how many suggestions should be shown in the list?
		},
		-- the presets plugin, adds help for a bunch of default keybindings in Neovim
		-- No actual key bindings are created
		presets = {
			operators = false, -- adds help for operators like d, y, ... and registers them for motion / text object completion
			motions = true, -- adds help for motions
			text_objects = false, -- help for text objects triggered after entering an operator
			windows = true, -- default bindings on <c-w>
			nav = true, -- misc bindings to work with windows
			z = true, -- bindings for folds, spelling and others prefixed with z
			g = true, -- bindings for prefixed with g
		},
	},
	-- add operators that will trigger motion and text object completion
	-- to enable all native operators, set the preset / operators plugin above
	-- operators = { gc = "Comments" },
	key_labels = {
		-- override the label used to display some keys. It doesn't effect WK in any other way.
		-- For example:
		-- ["<space>"] = "SPC",
		-- ["<CR>"] = "RET",
		-- ["<tab>"] = "TAB",
	},
	icons = {
		breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
		separator = "➜", -- symbol used between a key and it's label
		group = "+", -- symbol prepended to a group
	},
	popup_mappings = {
		scroll_down = "<c-d>", -- binding to scroll down inside the popup
		scroll_up = "<c-u>", -- binding to scroll up inside the popup
	},
	window = {
		border = "rounded", -- none, single, double, shadow
		position = "bottom", -- bottom, top
		margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
		padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
		winblend = 0,
	},
	layout = {
		height = { min = 4, max = 25 }, -- min and max height of the columns
		width = { min = 20, max = 50 }, -- min and max width of the columns
		spacing = 3, -- spacing between columns
		align = "left", -- align columns left, center or right
	},
	ignore_missing = true, -- enable this to hide mappings for which you didn't specify a label
	hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " }, -- hide mapping boilerplate
	show_help = true, -- show help message on the command line when the popup is visible
	triggers = "auto", -- automatically setup triggers
	-- triggers = {"<leader>"} -- or specify a list manually
	triggers_blacklist = {
		-- list of mode / prefixes that should never be hooked by WhichKey
		-- this is mostly relevant for key maps that start with a native binding
		-- most people should not need to change this
		i = { "j", "k" },
		v = { "j", "k" },
	},
}

local opts = {
	mode = "n", -- NORMAL mode
	prefix = "<leader>",
	buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
	silent = true, -- use `silent` when creating keymaps
	noremap = true, -- use `noremap` when creating keymaps
	nowait = true, -- use `nowait` when creating keymaps
}

local mappings = {
	["/"] = { '<cmd>lua require("Comment.api").toggle.linewise.current()<CR>', "Toggle comment" },
	["A"] = { "<cmd>Alpha<CR>", "Main Title" },
	["e"] = { "<cmd>NvimTreeToggle<CR>", "Nvim-tree explorer" },
	["x"] = { "<cmd>:bp|sp|bn|bd<CR>", "Close buffer" },
	["h"] = { "<cmd>nohlsearch<CR>", "No highlight" },
	c = {
		name = "crates",
		i = { "<cmd>lua require('crates').show_versions_popup()<CR>", "Show versions popup" },
		D = { "<cmd>lua require('crates').show_dependencies_popup()<CR>", "Show dependencies popup" },
		d = { "<cmd>lua require('crates').open_documentation()<CR>", "Open documentation" },
		f = { "<cmd>lua require('crates').show_features_popup()<CR>", "Show features popup" },
		g = { "<cmd>lua require('crates').open_repository()<CR>", "Open repository" },
		h = { "", "Open homepage" },

		r = { "<cmd>lua require('crates').reload()<CR>", "Reload" },
		u = { "<cmd>lua require('crates').update_crate()<CR>", "Update crate" },
		U = { "<cmd>lua require('crates').update_all_crates()<CR>", "Update all crates" },
		v = { "<cmd>lua require('crates').upgrade_crate()<CR>", "Upgrade crate" },
		V = { "<cmd>lua require('crates').upgrade_all_crates()<CR>", "Upgrade all crates" },
	},

	d = {
		name = "Doge",
		g = { "<cmd>DogeGenerate<Cr>", "Generate Doc" },
	},
	f = {
		name = "Telescope fzf",
		b = {
			"<cmd>Telescope buffers theme=ivy enable_preview=true layout_config={prompt_position='top'}<CR>",
			"Buffers",
		},
		C = { "<cmd>Telescope git_commits<CR>", "Commits" },
		c = { "<cmd>Telescope commands<CR>", "Commands" },
		D = { "<cmd>Telescope diagnostics<cr>", "All diagnostics" },
		d = { "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", "Buffer diagnostics" },
		f = {
			"<cmd>Telescope find_files hidden=true theme=ivy enable_preview=true layout_config={prompt_position='top}<CR>",
			"Find files",
		},
		g = { "<cmd>Telescope live_grep theme=ivy<CR>", "find grep" },
		h = { "<cmd>Telescope help_tags theme=ivy<CR>", "help tags" },
		k = { "<cmd>Telescope keymaps<CR>", "Keymaps" },
		m = { "<cmd>Telescope man_pages<CR>", "Man pages" },
		p = { "<cmd>Telescope planets theme=ivy<CR>", "use the telescope" },
		r = { "<cmd>Telescope oldfiles<CR>", "Recent files" },
		s = { "<cmd>Telescope git_status<CR>", "Open changed file" },
		v = { "<cmd>Telescope lsp_document_symbols<CR>", "Document symbols" },
		V = {
			"<cmd>Telescope lsp_dynamic_workspace_symbols<CR>",
			"Workspace symbols",
		},
	},
	g = {
		name = "Go to...",
		d = { "<cmd>lua vim.lsp.buf.definition()<CR>", "... definition" },
		g = { "gg", "... top" },
		G = { "G", "... bottom" },
	},
	G = {
		name = "Git",
		g = { "<cmd>lua _LAZYGIT_TOGGLE()<CR>", "Lazygit" },
		j = { "<cmd>lua require 'gitsigns'.next_hunk()<CR>", "Next Hunk" },
		k = { "<cmd>lua require 'gitsigns'.prev_hunk()<CR>", "Prev Hunk" },
		l = { "<cmd>lua require 'gitsigns'.blame_line()<CR>", "Blame" },
		p = { "<cmd>lua require 'gitsigns'.preview_hunk()<CR>", "Preview Hunk" },
		r = { "<cmd>lua require 'gitsigns'.reset_hunk()<CR>", "Reset Hunk" },
		R = { "<cmd>lua require 'gitsigns'.reset_buffer()<CR>", "Reset Buffer" },
		s = { "<cmd>lua require 'gitsigns'.stage_hunk()<CR>", "Stage Hunk" },
		u = {
			"<cmd>lua require 'gitsigns'.undo_stage_hunk()<CR>",
			"Undo Stage Hunk",
		},
		d = {
			"<cmd>Gitsigns diffthis HEAD<CR>",
			"Diff",
		},
	},

	l = {
		name = "LSP",
		d = { "<cmd>lua vim.lsp.buf.definition()<CR>", "Go to definition" },
		f = { "<cmd>lua vim.lsp.buf.format({async = true})<CR>", "Format" },
		g = { "<cmd>lua vim.diagnostic.open_float({{ border = 'rounded'}})<CR>", "Good luck..." },
		h = { "<cmd>lua vim.lsp.buf.hover()<CR>", "Hover information" },
		i = { "<cmd>lua vim.lsp.buf.implementation()<CR>", "Implementation" },
		j = {
			"<cmd>lua vim.diagnostic.goto_next()<CR>",
			"Next diagnostic",
		},
		k = {
			"<cmd>lua vim.diagnostic.goto_prev()<CR>",
			"Previous diagnostic",
		},
		L = { "<cmd>LspInstallInfo<CR>", "LSP installer info" },
		l = { "<cmd>LspInfo<CR>", "LSP file info" },
		n = { "<cmd>TodoTelescope<CR>", "To-Do Comments" },
		r = {
			"<cmd>lua vim.lsp.buf.references()<CR>",
			"References",
		},
		s = { "<cmd>lua vim.lsp.buf.signature_help()<CR>", "Signature help" },
		q = { "<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>", "Quickfix" },
	},
	n = {
		name = "Neovim",
		h = { ":checkhealth<cr>", "Health" },
	},
	p = {
		name = "Packer",
		c = { "<cmd>PackerCompile<CR>", "Compile" },
		i = { "<cmd>PackerInstall<CR>", "Install" },
		S = { "<cmd>PackerStatus<CR>", "Status" },
		s = { "<cmd>PackerSync<CR>", "Sync" },
		u = { "<cmd>PackerUpdate<CR>", "Update" },
	},
	s = {
		name = "TreeSitter",
		h = { "<cmd>TSToggle highlight<CR>", "Toggle highlight" },
		u = { "<cmd>TSUpdate all<CR>", "Update all" },
	},
	t = {
		name = "Terminal",
		f = { "<cmd>ToggleTerm direction=float<CR>", "Floating terminal" },
		h = { "<cmd>ToggleTerm size=10 direction=horizontal<CR>", "Horizontal terminal" },
		l = { "<cmd>lua _LAZYGIT_TOGGLE()<CR>", "Lazygit" },
		p = { "<cmd>lua _PYTHON_TOGGLE()<CR>", "Python" },
		t = { "<cmd>lua _HTOP_TOGGLE()<CR>", "htop" },
		u = { "<cmd>lua _NCDU_TOGGLE()<CR>", "NCDU" },
		v = { "<cmd>ToggleTerm size=80 direction=vertical<CR>", "Vertical terminal" },
	},
}

local vopts = {
	mode = "v", -- VISUAL mode
	prefix = "<leader>",
	buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
	silent = true, -- use `silent` when creating keymaps
	noremap = true, -- use `noremap` when creating keymaps
	nowait = true, -- use `nowait` when creating keymaps
}
local vmappings = {
	["/"] = { '<ESC><CMD>lua require("Comment.api").toggle.linewise_op(vim.fn.visualmode())<CR>', "Toggle comment" },
}
which_key.setup(setup)
which_key.register(mappings, opts)
which_key.register(vmappings, vopts)
