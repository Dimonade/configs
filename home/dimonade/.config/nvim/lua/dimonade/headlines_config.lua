local status_ok, headlines = pcall(require, "headlines")
if not status_ok then
    vim.notify("headlines has failed to initialzie.")
    return
end

headlines.setup({
    markdown = {
        headline_highlights = { "Headline1", "Headline2", "Headline3", "Headline4", "Headline5", "Headline6" },
        codeblock_highlight = "CodeBlock",
        dash_highlight = "Dash",
        quote_highlight = "Quote"
    },
})
