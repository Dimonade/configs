local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	vim.notify("packer has failed to initialize.")
	return
end

-- Have packer use a popup window.
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

return packer.startup(function(use)
	use("wbthomason/packer.nvim") -- Have packer manage itself.
	use("nvim-lua/popup.nvim")
	use("nvim-lua/plenary.nvim") -- Useful lua functions.
	use("windwp/nvim-autopairs") -- Autopairs, integrates with cmp and treesitter
	use("numToStr/Comment.nvim") -- Easily comment stuff
	use("folke/todo-comments.nvim")
	use({ "akinsho/bufferline.nvim", tag = "v3.*", requires = "nvim-tree/nvim-web-devicons" })
	use({
		"nvim-lualine/lualine.nvim",
		requires = { "kyazdani42/nvim-web-devicons", opt = true },
	})
	use("akinsho/toggleterm.nvim")
	use({
		"goolord/alpha-nvim",
		requires = { "kyazdani42/nvim-web-devicons" },
	})
	use("folke/which-key.nvim")
	use("shaunsingh/nord.nvim")
	use("lukas-reineke/headlines.nvim")
	use("hrsh7th/nvim-cmp") -- The completion plugin.
	use("hrsh7th/cmp-buffer") -- buffer completions.
	use("hrsh7th/cmp-path") -- path completions.
	use("hrsh7th/cmp-cmdline") -- cmdline completions.
	use("hrsh7th/cmp-nvim-lsp")
	use("hrsh7th/cmp-nvim-lua")
	use("saadparwaiz1/cmp_luasnip") -- snippet completions.
	use("Saecki/crates.nvim") -- Rust Crates.
	use("hrsh7th/cmp-calc") -- evaluating mathematical expressions.
	use("lukas-reineke/cmp-rg") -- ripgrep
	use("L3MON4D3/LuaSnip") --snippet engine.
	use("rafamadriz/friendly-snippets") -- a bunch of snippets to use.
	-- The order is important: mason, mason-lspconfig, nvim-lspconfig.
	use("williamboman/mason.nvim") -- LSP.
	use("williamboman/mason-lspconfig.nvim")
	use("neovim/nvim-lspconfig") -- enable LSP.
	use("jose-elias-alvarez/null-ls.nvim") -- for formatters and linters.
	use("mfussenegger/nvim-dap") -- Debug Adapter Protocol.
	use("nvim-telescope/telescope.nvim")
	use({
		"nvim-treesitter/nvim-treesitter",
		run = ":TSUpdate",
	})
    use("shellRaining/hlchunk.nvim")
	use("JoosepAlviste/nvim-ts-context-commentstring")
	use("lewis6991/gitsigns.nvim")
	use({
		"kyazdani42/nvim-tree.lua",
		requires = {
			"kyazdani42/nvim-web-devicons", -- optional, for file icon
		},
	})
	use({ "wfxr/minimap.vim", requires = { "wfxr/code-minimap" } }) -- Minimap.
	use({
		"kkoomen/vim-doge",
		run = ":call doge#install()",
	})
	use("lervag/vimtex")
    use("rust-lang/rust.vim")
	use("simrat39/rust-tools.nvim")
	use("RRethy/vim-illuminate")
    use("mbbill/undotree")
    use("ThePrimeagen/vim-be-good")

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
