-- :help options
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.clipboard = "unnamedplus"
vim.opt.cmdheight = 2
vim.opt.encoding = "utf-8"
vim.opt.fileencoding = "utf-8"
vim.opt.completeopt = { "menuone", "noselect" }
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.tabstop = 4
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.redrawtime = 500
vim.opt.emoji = false
vim.opt.colorcolumn = "88"
vim.opt.termguicolors = true
vim.opt.swapfile = false
vim.opt.mouse = "a"
vim.opt.undofile = true
vim.opt.foldmethod = "indent"
vim.opt.timeoutlen = 300
vim.opt.termguicolors = true

vim.opt.listchars = { eol = "↓", space = "·", trail = "~", tab = ">>>" }
vim.opt.list = true

vim.cmd([[highlight ColorColumn ctermbg=lightgrey guibg=lightgrey]])
-- LaTeX, probably enabled by default.
vim.api.nvim_command("filetype plugin indent on")
-- `syntax enable` is enabled by default.

vim.api.nvim_command("autocmd BufRead,BufNewFile /tmp/calcurse* set filetype=markdown")
vim.api.nvim_command("autocmd BufRead,BufNewFile ~/.calcurse/notes/* set filetype=markdown")
vim.api.nvim_command("autocmd BufRead,BufNewFile */\\cjenkinsfile set filetype=groovy")
vim.api.nvim_command("autocmd BufRead,BufNewFile *.kdl set filetype=groovy")

vim.g.rustfmt_autosave = 1
