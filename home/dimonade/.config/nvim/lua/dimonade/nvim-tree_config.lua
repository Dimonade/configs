vim.g.auto_close = false
vim.g.auto_resize = true
vim.g.disable_window_picker = 0
vim.g.root_folder_modifier = ":t"
vim.g.quit_on_open = 0
vim.g.git_hl = 1
vim.g.show_icons = {
	git = 1,
	folders = 1,
	files = 1,
	folder_alrows = 1,
	tree_width = 30,
}
vim.g.update_to_buf_dir = {
	enable = false,
	auto_open = true,
}

local status_ok, nvim_tree = pcall(require, "nvim-tree")
if not status_ok then
	vim.notify("nvim-tree has failed to initialize.")
	return
end

nvim_tree.setup({
	disable_netrw = true,
	hijack_netrw = true,
	open_on_tab = false,
	hijack_cursor = false,
	update_cwd = true,
	diagnostics = {
		enable = true,
		icons = {
			hint = "",
			info = "",
			warning = "",
			error = "",
		},
	},
	update_focused_file = {
		enable = false,
		update_cwd = false,
		ignore_list = {},
	},

	renderer = {
		icons = {
			glyphs = {
				default = "",
				symlink = "",
				git = {
					unstaged = "",
					staged = "",
					unmerged = "",
					renamed = "",
					untracked = "",
					deleted = "✖",
					ignored = "",
				},
				folder = {
					arrow_open = "",
					arrow_closed = "",
					default = "",
					open = "",
					empty = "",
					empty_open = "",
					symlink = "",
					symlink_open = "",
				},
			},
		},
	},
	git = {
		enable = true,
		ignore = true,
		timeout = 500,
	},
	view = {
		width = 30,
		hide_root_folder = false,
		side = "left",
		number = false,
		relativenumber = false,
	},
})
