local status_ok, hlchunk = pcall(require, "hlchunk")
if not status_ok then
	vim.notify("hlchunk has failed to initialize.")
end

hlchunk.setup({
	chunk = {
		enable = true,
		support_filetype = {
			"*.rs",
			"*.lua",
			"*.py",
			"*.tex",
			"*.html",
			"*.json",
			"*.c",
			"*.h",
			"*.cpp",
		},
		chars = {
			horizontal_line = "─",
			vertical_line = "│",
			left_top = "╭",
			left_bottom = "╰",
			right_arrow = ">",
		},
		style = "#806d9c",
	},
	line_num = {
		style = "#806d9c",
	},
})
