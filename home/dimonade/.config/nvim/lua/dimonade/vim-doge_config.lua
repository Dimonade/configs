vim.g.doge_doc_standard_python = 'numpy'
vim.g.doge_doc_standard_php = 'phpdoc'
vim.g.doge_doc_standard_javascript = 'jsdoc'
vim.g.doge_doc_standard_typescript = 'jsdoc'
vim.g.doge_doc_standard_lua = 'ldoc'
vim.g.doge_doc_standard_java = 'javadoc'
vim.g.doge_doc_standard_groovy = 'javadoc'
vim.g.doge_doc_standard_ruby = 'YARD'
vim.g.doge_doc_standard_cpp = 'doxygen_qt'
vim.g.doge_doc_standard_c = 'doxygen_qt'
vim.g.doge_doc_standard_sh = 'google'
vim.g.doge_doc_standard_rs = 'rustdoc'
vim.g.doge_doc_standard_cs = 'xmldoc'
