local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
	vim.notify("nvim-treesitter has failed to initialize.")
	return
end

configs.setup({
	ensure_installed = {
		"python",
		"rust",
		"latex",
		"bash",
		"c",
		"cpp",
		"gdscript",
		"html",
		"lua",
		"make",
		"markdown",
		"norg",
		"pascal",
		"regex",
		"sql",
		"toml",
		"vim",
		"yaml",
	},
	sync_install = false,
	autopairs = {
		enable = true,
	},
	highlight = {
		enable = true, -- false will disable the whole extension
		disable = {}, -- list of language that will be disabled
		additional_vim_regex_highlighting = false,
	},
	indent = { enable = true, disable = { "yaml", "python", "latex" } },
	context_commentstring = {
		enable = true,
		enable_autocmd = false,
	},
})
