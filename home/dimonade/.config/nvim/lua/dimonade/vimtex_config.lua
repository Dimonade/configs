vim.g.vimtex_view_method = "zathura"
vim.g.vimtex_quickfix_mode = 0
vim.g.vimtex_compiler_methon = "latexmk" -- Other options include `latexrun`.
